% Copyright 2017 Mechanics for Material Design Lab,
% Sibley School of Mechanical and Aerospace Engineering,
% Cornell, Ithaca
% Author: Meenakshi Sundaram
% Contact: mm2422 at cornell dot edu

% This file is part of RadialSymmetricPull package that solves the model in the paper
% "Theoretical framework and design of mechanochemically augmented polymer composites"
% by Meenakshi Sundaram Manivannan and Meredith N. Silberstein
% Cite the work using the following Bibtex entry
% @article{manivannan2018theoretical,
% title={Theoretical framework and design of mechanochemically augmented polymer composites},
% author={Manivannan, Meenakshi Sundaram and Silberstein, Meredith N},
% journal={Extreme Mechanics Letters},
% volume={19},
% pages={27--38},
% year={2018},
% publisher={Elsevier}
% }

% RigidPart is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License.
%
% RigidPart is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with RigidPart.
% If not, see <http://www.gnu.org/licenses/>.

% This file is a demo that generates the figures required for Fig.4 of the
% paper

%%
clc;clear;

%%
% Loading steps
nsteps=2048;
sigmainfty=linspace(0,.75,nsteps);

% Reference

%Matrix
Em=800;%MPa
num=0.3;

%Particle
Rp=13+1/3;

%Interface
delta=0.75;%nm
SigmaMax=0.15;

%Interphase
n=0;

% Case 1
SigmaMax1=0.30;

% Case 2
num2=0.4;

% Case 3
Rp3=(13+1/3)*1E3;

% Convertion to plane stress, actual values

% Reference
Emconv=Em/(1-num^2);
numconv=num./(1-num);

phi=exp(1)*delta*SigmaMax*Em;
matpropref=[Emconv,n,numconv,SigmaMax*Em,phi];
geompropref=[Rp*delta,Rp*delta];

%Case 1
phi1=exp(1)*delta*SigmaMax1*Em;
matprop1=[Emconv,n,numconv,SigmaMax1*Em,phi1];
geomprop1=[Rp*delta,Rp*delta];

%Case 2
Emconv2=Em/(1-num2^2);
numconv2=num2./(1-num2);
matprop2=[Emconv2,n,numconv2,SigmaMax*Em,phi];
geomprop2=[Rp*delta,Rp*delta];

%Case 3
matprop3=[Emconv,n,numconv,SigmaMax*Em,phi];
geomprop3=[Rp3*delta,Rp3*delta];

%% Solve the mechanics
debond=zeros(nsteps,4);
coeffs=repmat(struct('As',[0,0],'Abars',[0,0],'Cs',[0,0],'Cbars',[0,0]),nsteps,4);
for iind = 1:nsteps
    sigmas=[0,sigmainfty(iind)]*Em;
    [debond(iind,1),coeffs(iind,1)]=rigmechanics(sigmas,matpropref,geompropref);
    [debond(iind,2),coeffs(iind,2)]=rigmechanics(sigmas,matprop1,geomprop1);
    [debond(iind,3),coeffs(iind,3)]=rigmechanics(sigmas,matprop2,geomprop2);
    [debond(iind,4),coeffs(iind,4)]=rigmechanics(sigmas,matprop3,geomprop3);
end

%%
poa=[find(debond(:,1)>=delta,1);find(debond(:,2)>=delta,1);find(debond(:,3)>=delta,1);find(debond(:,4)>=delta,1)];
fsize=32;lsize=4;asize=2;
h1=figure(1);clf;hold on;
set(gca,'fontsize',fsize,'linewidth',asize);

plot(sigmainfty,2.0*sigmainfty*(1.0-num^2)*Rp,'-','linewidth',lsize);
plot(sigmainfty,debond(:,1)/delta,'-','linewidth',lsize);
plot(sigmainfty,debond(:,2)/delta,'-k','linewidth',lsize);
plot(sigmainfty(32:32:end),debond(32:32:end,2)/delta,'linestyle','none','Marker','o','MarkerSize',10,'MarkerFaceColor','k','MarkerEdgeColor','k');
plot(sigmainfty,debond(:,3)/delta,'--k','linewidth',lsize);
plot(sigmainfty,debond(:,4)/delta,':k','linewidth',lsize);

axis([0,0.5*sigmainfty(end),0,12])
plot(xlim,[1,1],'--k','linewidth',lsize/2);
axis square;
box on;
xlabel('$\Sigma_{\infty}$','interpreter','LaTeX');
ylabel('$\left[U\right]$','interpreter','LaTeX','Rotation',0);
%print(h1,'figures/figure1.svg','-dsvg')
%%
% Mechanophore cases 
Lna1=4/3; DeltaL1=0.2;
Lna2=4/3; DeltaL2=0.8;
Lna3=4/3; DeltaL3=2.0;
Lna4=8/3; DeltaL4=0.2;

lna=delta*[Lna1,Lna2,Lna3,Lna4];
la=delta*[Lna1+DeltaL1,Lna2+DeltaL2,Lna3+DeltaL3,Lna4+DeltaL4];
ra=Rp*delta-0.1;%nm
mphoreprop=[lna(1),la(1),ra;...
    lna(2),la(2),ra;...
    lna(3),la(3),ra;...
    lna(4),la(4),ra];
    
numsamples=128;
act=zeros(4,nsteps);
act2stress=zeros(4,numsamples);
phisamples=zeros(4,numsamples);
for iind = 1:nsteps
    sigmas=[0,sigmainfty(iind)]*Em;
    if(iind==1)
        [act(1,iind),whichact,phisamples(1,:)]=compurigactiv(sigmas,matpropref,geompropref,mphoreprop(1,:),coeffs(iind,1),numsamples);
        act2stress(1,whichact)=sigmainfty(iind);
        [act(2,iind),whichact,phisamples(2,:)]=compurigactiv(sigmas,matpropref,geompropref,mphoreprop(2,:),coeffs(iind,1),numsamples);
        act2stress(2,whichact)=sigmainfty(iind);
        [act(3,iind),whichact,phisamples(3,:)]=compurigactiv(sigmas,matpropref,geompropref,mphoreprop(3,:),coeffs(iind,1),numsamples);
        act2stress(3,whichact)=sigmainfty(iind);
        [act(4,iind),whichact,phisamples(4,:)]=compurigactiv(sigmas,matpropref,geompropref,mphoreprop(4,:),coeffs(iind,1),numsamples);
        act2stress(4,whichact)=sigmainfty(iind);
    else
        [act(1,iind),whichact,~]=compurigactiv(sigmas,matpropref,geompropref,mphoreprop(1,:),coeffs(iind,1),numsamples,phisamples(1,:));
        loc=act2stress(1,:)==0 & whichact;
        act2stress(1,loc)=sigmainfty(iind);
        [act(2,iind),whichact,~]=compurigactiv(sigmas,matpropref,geompropref,mphoreprop(2,:),coeffs(iind,1),numsamples,phisamples(2,:));
        loc=act2stress(2,:)==0 & whichact;
        act2stress(2,loc)=sigmainfty(iind);
        [act(3,iind),whichact,~]=compurigactiv(sigmas,matpropref,geompropref,mphoreprop(3,:),coeffs(iind,1),numsamples,phisamples(3,:));
        loc=act2stress(3,:)==0 & whichact;
        act2stress(3,loc)=sigmainfty(iind);
        [act(4,iind),whichact,~]=compurigactiv(sigmas,matpropref,geompropref,mphoreprop(4,:),coeffs(iind,1),numsamples,phisamples(4,:));
        loc=act2stress(4,:)==0 & whichact;
        act2stress(4,loc)=sigmainfty(iind);
    end
end
%%
fsize=32;lsize=4;asize=2;
h1=figure(2);clf;hold on;
yyaxis left;
ax1=plot(sigmainfty,debond(:,1)/delta,'linewidth',lsize,'linestyle','-');
plot(xlim,[1,1],'--k','linewidth',lsize/2);
set(ax1.Parent,'fontsize',fsize,'linewidth',asize);
ax1.Parent.YLim=[0,12];
set(ax1.Parent,'fontsize',fsize,'linewidth',asize);
ylabel('$\left[U\right]$','Interpreter','LaTeX','Rotation',0)
yyaxis right;
ax2=plot(sigmainfty,act(1,:),'linewidth',lsize,'linestyle','-');
loc=[1:25:179,180:3:219,220:25:nsteps];
ax3=plot(sigmainfty(loc),act(2,loc),'linewidth',lsize,...
    'linestyle','none','Marker','o','MarkerSize',15,'MarkerFaceColor',ax2.Color,'MarkerEdgeColor','none');
ax4=plot(sigmainfty,act(3,:),'linewidth',lsize,'linestyle',':');
ax5=plot(sigmainfty,act(4,:),'linewidth',lsize,'linestyle','--');
set(ax2.Parent,'fontsize',fsize,'linewidth',asize,'YLim',[-1.0,2.0],'YTick',[0,0.25,0.5,0.75,1],'XLim',[0,0.5*sigmainfty(end)])
xlabel('$\Sigma_{\infty}$','Interpreter','LaTeX','fontsize',fsize);
ylabel('$\mathcal{E}$','Interpreter','LaTeX','fontsize',fsize,'Rotation',0)
axis square;
box on;
%print(h1,'figures/figure2.svg','-dsvg')

%%
fsize=32;lsize=4;asize=2;
h1=figure(3);clf;hold on;
box on;
yyaxis left;
ax1=plot(sigmainfty,debond(:,1)/delta,'linewidth',lsize,'linestyle','-');
plot(xlim,[1,1],'--k','linewidth',lsize/2);
set(ax1.Parent,'fontsize',fsize,'linewidth',asize);
ax1.Parent.YLim=[0,4];
set(ax1.Parent,'fontsize',fsize,'linewidth',asize);
ylabel('$\left[U\right]$','Interpreter','LaTeX','Rotation',0)
yyaxis right;
ax2=plot(act2stress(1,:),abs(phisamples(1,:))*180/pi,'linewidth',lsize,'linestyle','-');
%ax3=plot(act2stress(2,1:6:end),abs(phisamples(2,1:6:end))*180/pi,'linewidth',lsize,...
%    'linestyle','none','Marker','o','MarkerSize',15,'MarkerFaceColor',ax2.Color,'MarkerEdgeColor','none');
%ax4=plot(act2stress(3,:),abs(phisamples(3,:))*180/pi,'linewidth',lsize,'linestyle',':');
ax2=plot(act2stress(4,:),abs(phisamples(4,:))*180/pi,'linewidth',lsize,'linestyle','--');
set(ax2.Parent,'fontsize',fsize,'linewidth',asize,'YLim',[-30,135],'YTick',[0,45,90],'XLim',[0,0.25*sigmainfty(end)])
xlabel('$\Sigma_{\infty}$','Interpreter','LaTeX','fontsize',fsize);
ylabel('$\left|\phi\right|$','Interpreter','LaTeX','fontsize',fsize,'Rotation',0)
axis square;
%print(h1,'figures/figure3.svg','-dsvg')

%% Phase map
Ra=ra/delta;
Sigma_max=linspace(0.01,0.3750,nsteps);
DeltaLactb4deb=sqrt((1+Rp+Lna1*Lna1)-Ra*Ra/Rp+Lna1*Lna1/Rp)-Lna1;
DeltaLacta4deb=1/(2*Rp)*((1-2.0*num)/(1.0-num)*(Ra+Lna1)+Rp*Rp*(1/(1-num))/(Ra+Lna1))+...
    Sigma_max*((1-2*num)*(1+num)/(1-num))*((Ra+Lna1)-Rp*Rp/(Ra+Lna1))/2.0;
%%
lsize=4;
h1=figure(4);clf;set(gca,'fontsize',fsize,'LineWidth',asize);hold on;
plot(Sigma_max,DeltaLactb4deb*ones(size(Sigma_max)),'linewidth',lsize,'color','k');
plot(Sigma_max,DeltaLacta4deb,'linewidth',lsize,'color','k');
axis([0,Sigma_max(end),0,2])
xlabel('$\Sigma_{\max}$','Interpreter','LaTeX','fontsize',fsize);
ylabel('$\Delta L$','Interpreter','LaTeX','fontsize',fsize,'Rotation',0);
axis square;box on;
%print(h1,'figures/figure4.svg','-dsvg')