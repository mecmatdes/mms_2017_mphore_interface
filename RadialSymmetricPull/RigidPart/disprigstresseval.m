function [disp,stress]=disprigstresseval(matprop,geomprop,sigmas,As,Abars,Cs,Cbars,rvals)
%
%Input
%
%   matprop     -   material properties 
%               [1,2,3,4,5,6,7]
%               1. Ea      -   Youngs modulus of Interphase at ra
%               3. n       -   power law coefficient n
%               4. num     -   poisson's ratio of matrix   
%               6. sigmamax-   max stress of the cohesive law of the interface
%               7. phitot  -   energy of the cohesive law of the interface 
%   geomprop    -   geometric properties
%               [1,2]
%               ra      -   radius at the beginning of Interphase
%               rb      -   radius at the end of Interphase
%   sigmas      -   stresses involved
%               [1,2]
%               sigmaz  -   stress along z direction
%               sigmab  -   stress along the radial direction
%   As          -   Displacement Coefficients in fgvi
%                   A1,A2
%   Abars       -   Stress Coefficients in fgvi
%                   Abar1 Abar2
%   Cs          -   Displacement Coefficients in HM
%                   C1,C2
%   Cbars       -   Stress Coefficients in HM
%                   Cbar1,Cbar2
%   rvals       -   radius values beyond ra at which displacement needs to
%                   be evaluated
%Output
%   
%   disp        -   [...] 1xnsamples    displacement along r
%   stress      -   [...                radial stress
%                    ...] 2xnsamples    tangential stress

Ea=matprop(1);n=matprop(2);num=matprop(3);sigmamax=matprop(4);phitot=matprop(5);
ra=geomprop(1);rb=geomprop(2);
sigmaz=sigmas(1);

A1=As(1);A2=As(2);
Abar1=Abars(1);Abar2=Abars(2);
C1=Cs(1);C2=Cs(2);
Cbar1=Cbars(1);Cbar2=Cbars(2);

% Parametrize k
k=sqrt(n*n+4.0*(1-n*num));

%
nsamples=length(rvals);

% Generate radial samples
disp=zeros(1,nsamples);
stress=zeros(2,nsamples);

% Locate FGVI zone and find displacements and stresses
loc=(rvals<rb);
disp(loc)=rvals(loc).^(-n/2.0).*(A1*rvals(loc).^(k/2.0)+A2*rvals(loc).^(-k/2.0));
stress(1,loc)=(rvals(loc).^(n/2.0-1.0)/ra^n).*(Abar1*rvals(loc).^(k/2.0)*(k-n+2.0*num)+Abar2*rvals(loc).^(-k/2.0)*(-k-n+2.0*num))+sigmaz*num/(1-num);
stress(2,loc)=(rvals(loc).^(n/2.0-1.0)/ra^n).*(Abar1*rvals(loc).^(k/2.0)*(2.0+num*(k-n))+Abar2*rvals(loc).^(-k/2.0)*(2.0+num*(-k-n)))+sigmaz*num/(1-num);

% Locate HM zone and find displacement and stresses
loc=(rvals>=rb);
disp(loc)=C1*rvals(loc)+C2./rvals(loc);
stress(1,loc)=Cbar1*(1.0+num)-Cbar2*(1.0-num)./(rvals(loc).^2)+sigmaz*num/(1.0-num);
stress(2,loc)=Cbar1*(1.0+num)+Cbar2*(1.0-num)./(rvals(loc).^2)+sigmaz*num/(1.0-num);