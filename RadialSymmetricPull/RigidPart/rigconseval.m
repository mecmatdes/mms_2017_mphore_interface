% Copyright 2017 Mechanics for Material Design Lab,
% Sibley School of Mechanical and Aerospace Engineering,
% Cornell, Ithaca
% Author: Meenakshi Sundaram
% Contact: mm2422 at cornell dot edu

% This file is part of RadialSymmetricPull package that solves the model in the paper
% "Theoretical framework and design of mechanochemically augmented polymer composites"
% by Meenakshi Sundaram Manivannan and Meredith N. Silberstein
% Cite the work using the following Bibtex entry
% @article{manivannan2018theoretical,
% title={Theoretical framework and design of mechanochemically augmented polymer composites},
% author={Manivannan, Meenakshi Sundaram and Silberstein, Meredith N},
% journal={Extreme Mechanics Letters},
% volume={19},
% pages={27--38},
% year={2018},
% publisher={Elsevier}
% }

% RigidPart is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License.
%
% RigidPart is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with RigidPart.
% If not, see <http://www.gnu.org/licenses/>.
function [As,Abars,Cs,Cbars]=rigconseval(u,matprop,geomprop,sigmas)
% Function evaluates the constants necessary to expand displacements
% everywhere on the infinite matrix given the interface displacement, 
% material properties, geometric properties & remote loading.
%
% The system whose diplacements are solved for is an infinite matrix with a
% rigid circular particle subjective to a cohesive zone law. This system is 
% remotely loaded by a stress state. The interface has a varying Young's 
% modulus forming an interphase.
% Input
%   u           -   debonding displacement
%   matprop     -   material properties 
%               [1,2,3,4,5,6,7]
%               1. Ea      -   Youngs modulus of Interphase at ra
%               3. n       -   power law coefficient n
%               4. num     -   poisson's ratio of matrix   
%               6. sigmamax-   max stress of the cohesive law of the interface
%               7. phitot  -   energy of the cohesive law of the interface 
%   geomprop    -   geometric properties
%               [1,2]
%               ra      -   radius at the beginning of Interphase
%               rb      -   radius at the end of Interphase
%   sigmas      -   stresses involved
%               [1,2]
%               sigmaz  -   stress along z direction
%               sigmab  -   stress along the radial direction
% Output
%   As          -   Displacement Coefficients in fgvi
%                   A1,A2
%   Abars       -   Stress Coefficients in fgvi
%                   Abar1 Abar2
%   Cs          -   Displacement Coefficients in HM
%                   C1,C2
%   Cbars       -   Stress Coefficients in HM
%                   Cbar1,Cbar2

% Unpack the properties
Ea=matprop(1);n=matprop(2);num=matprop(3);sigmamax=matprop(4);phitot=matprop(5);
ra=geomprop(1);rb=geomprop(2);
sigmaz=sigmas(1);sigmab=sigmas(2);
Eb=Ea*(rb/ra)^n;

% Cohesive zone stress evaluation
[sigmaint,~]=cohesivelaw(u,sigmamax,phitot);

% Parametrize k
k=sqrt(n*n+4.0*(1-n*num));

% Constant As, Abars
A2=rb^((n+k)/2.0-1.0)*2.0*(sigmaint*(1-num*num)-sigmaz*(1.0+num)*num)*(k-n+2.0)...
    -ra^((n+k)/2.0-1.0)*4.0*(sigmab*(1.0-num)-sigmaz*num)*(k-n+2.0*num);
A2=A2/Ea;
A2=A2/(ra^(-(n+k)/2.0-1.0)*rb^((n+k)/2.0-1.0)*(-k-n+2.0*num)*(k-n+2.0)...
    -ra^((-n+k)/2.0-1.0)*rb^((n-k)/2.0-1.0)*(k-n+2.0*num)*(-k-n+2.0));

A1=4.0*(ra^n)*(sigmab*(1.0-num)-sigmaz*num)-Ea*A2*rb^((n-k)/2.0-1.0)*(-k-n+2.0);
A1=A1/(Ea*rb^((n+k)/2.0-1.0)*(k-n+2.0));

sigmaint1=(Ea*rb^(n/2.0-1.0)*(A1*rb^(k/2.0)*(k-n+2.0*num)+A2*rb^(-k/2.0)*(-k-n+2.0*num))/(2.0*(ra^n)*(1.0-num*num)))+sigmaz*num/(1.0-num);

As = [A1,A2];

Abars=As*Ea/(2.0*(1.0-num^2));

% Constant Cs, Cbars
C1=(sigmab*(1.0-num)-sigmaz*num)/Eb;
C2=rb^2*(sigmab-sigmaint1)*(1.0+num)/Eb;
Cs=[C1,C2];
Cbars=Eb*Cs/(1.0-num*num);
