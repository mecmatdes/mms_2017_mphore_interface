% Copyright 2017 Mechanics for Material Design Lab,
% Sibley School of Mechanical and Aerospace Engineering,
% Cornell, Ithaca
% Author: Meenakshi Sundaram
% Contact: mm2422 at cornell dot edu

% This file is part of RadialSymmetricPull package that solves the model in the paper
% "Theoretical framework and design of mechanochemically augmented polymer composites"
% by Meenakshi Sundaram Manivannan and Meredith N. Silberstein
% Cite the work using the following Bibtex entry
% @article{manivannan2018theoretical,
% title={Theoretical framework and design of mechanochemically augmented polymer composites},
% author={Manivannan, Meenakshi Sundaram and Silberstein, Meredith N},
% journal={Extreme Mechanics Letters},
% volume={19},
% pages={27--38},
% year={2018},
% publisher={Elsevier}
% }

% PliablePart is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License.
%
% PliablePart is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with RigidPart.
% If not, see <http://www.gnu.org/licenses/>.
%
% This file is script that runs and generates the figure 6 of the paper

%%
clc;clear;
%%
% Loading steps
nsteps=1024;
sigmainfty=linspace(0,1.0,nsteps);

% Reference

%Matrix
Em=10E3;%MPa
num=0.3;

%Interface
delta=0.75;%nm
sigmamax=0.5*Em;%MPa

%Particle
rp=10;%nm
Rp=rp/delta; 
Ep1=500E3;%MPa Rigid
Ep2=70E3;%MPa Compliant
nup=0.3;

%Interphase
Erel=1.0;
rm=rp;
n=0.0;

% Convertion to plane stress, actual values

% Reference
Emconv=Em/(1-num^2);
numconv=num./(1-num);
Ep1conv=Ep1/(1-nup^2);
Ep2conv=Ep2/(1-nup^2);
nupconv=nup/(1-nup);

phi=exp(1)*delta*sigmamax;

matprop=zeros(2,7);
matprop(:,1)=[Emconv,Emconv]';
matprop(:,2)=[Ep1conv,Ep2conv]';
matprop(:,3)=n;
matprop(:,4)=numconv;
matprop(:,5)=nupconv;
matprop(:,6)=sigmamax;
matprop(:,7)=phi;
geomprop=[rp,rm;rp,rm];

%% Solve the mechanics
debond=zeros(nsteps,2);
coeffs=repmat(struct('As',[0,0],'Abars',[0,0],'Cs',[0,0],'Cbars',[0,0]),nsteps,2);
for iind = 1:nsteps
    sigmas=[0,sigmainfty(iind)]*Em;
    if(iind==1)
        [debond(iind,1),coeffs(iind,1)]=mechanics(sigmas,matprop(1,:),geomprop(1,:));
        [debond(iind,2),coeffs(iind,2)]=mechanics(sigmas,matprop(2,:),geomprop(2,:));
    else
        [debond(iind,1),coeffs(iind,1)]=mechanics(sigmas,matprop(1,:),geomprop(1,:),debond(iind-1,1));
        [debond(iind,2),coeffs(iind,2)]=mechanics(sigmas,matprop(2,:),geomprop(2,:),debond(iind-1,2));
    end
end
%% Activation for mechanophore

Lna1=4/3; DeltaL1=0.8;
Lna2=4/3; DeltaL2=2.0;

lna1=delta*Lna1;
lna2=delta*Lna2;

la1=lna1+DeltaL1*delta;
la2=lna2+DeltaL2*delta;

ra=rp-0.1;%nm
mphoreprop=[lna1,la1,ra;...
            lna2,la2,ra];

%
numsamples=128;
act=zeros(4,nsteps);
phisamples=zeros(4,numsamples);
for iind = 1:nsteps
    sigmas=[0,sigmainfty(iind)]*Em;
    if(iind==1)
        [act(1,iind),~,phisamples(1,:)]=compuactiv(sigmas,matprop(1,:),geomprop(1,:),mphoreprop(1,:),debond(iind,1),coeffs(iind,1),numsamples);
        [act(2,iind),~,phisamples(2,:)]=compuactiv(sigmas,matprop(1,:),geomprop(1,:),mphoreprop(2,:),debond(iind,1),coeffs(iind,1),numsamples);
        [act(3,iind),~,phisamples(3,:)]=compuactiv(sigmas,matprop(2,:),geomprop(2,:),mphoreprop(1,:),debond(iind,2),coeffs(iind,2),numsamples);
        [act(4,iind),~,phisamples(4,:)]=compuactiv(sigmas,matprop(2,:),geomprop(2,:),mphoreprop(2,:),debond(iind,2),coeffs(iind,2),numsamples);
    else
        [act(1,iind),~,~]=compuactiv(sigmas,matprop(1,:),geomprop(1,:),mphoreprop(1,:),debond(iind,1),coeffs(iind,1),numsamples,phisamples(1,:));
        [act(2,iind),~,~]=compuactiv(sigmas,matprop(1,:),geomprop(1,:),mphoreprop(2,:),debond(iind,1),coeffs(iind,1),numsamples,phisamples(2,:));
        [act(3,iind),~,~]=compuactiv(sigmas,matprop(2,:),geomprop(2,:),mphoreprop(1,:),debond(iind,2),coeffs(iind,2),numsamples,phisamples(3,:));
        [act(4,iind),~,~]=compuactiv(sigmas,matprop(2,:),geomprop(2,:),mphoreprop(2,:),debond(iind,2),coeffs(iind,2),numsamples,phisamples(4,:));
    end
end

%%
fsize=32;lsize=3;asize=2;
h1=figure(1);clf;hold on;
box on;
yyaxis left;
plot(sigmainfty,debond(:,1)/delta,'-','linewidth',lsize);
ax1=plot(sigmainfty,debond(:,2)/delta,'--','linewidth',lsize);
plot(xlim,[1,1],'--k','linewidth',lsize/2);
set(ax1.Parent,'fontsize',fsize,'linewidth',asize);

xlabel('$\Sigma_{\infty}$','interpreter','LaTeX');
ylabel('$\left[U\right]$','interpreter','LaTeX','Rotation',0);

yyaxis right
plot(sigmainfty,act(1,:),'-','linewidth',lsize);
ax2=plot(sigmainfty,act(3,:),'--','linewidth',lsize);

set(ax2.Parent,'fontsize',fsize,'linewidth',asize,'YLim',[-0.5,1.5],...
    'YTick',[0,0.25,0.5,0.75,1],'XLim',[0,0.75*sigmainfty(end)])
xlabel('$\Sigma_{\infty}$','Interpreter','LaTeX','fontsize',fsize);
ylabel('$\mathcal{E}$','Interpreter','LaTeX','fontsize',fsize,'Rotation',0)
axis square;
print(h1,'figures/figure1.svg','-dsvg')

%%
error1=simps(sigmainfty,(debond(:,1)-debond(:,2))/delta)/simps(sigmainfty,debond(:,1)/delta);
error2=simps(sigmainfty,(act(1,:)-act(3,:)))/simps(sigmainfty,act(1,:));