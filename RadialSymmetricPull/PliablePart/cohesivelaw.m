% Copyright 2017 Mechanics for Material Design Lab,
% Sibley School of Mechanical and Aerospace Engineering,
% Cornell, Ithaca
% Author: Meenakshi Sundaram
% Contact: mm2422 at cornell dot edu

% This file is part of RadialSymmetricPull package that solves the model in the paper
% "Theoretical framework and design of mechanochemically augmented polymer composites"
% by Meenakshi Sundaram Manivannan and Meredith N. Silberstein
% Cite the work using the following Bibtex entry
% @article{manivannan2018theoretical,
% title={Theoretical framework and design of mechanochemically augmented polymer composites},
% author={Manivannan, Meenakshi Sundaram and Silberstein, Meredith N},
% journal={Extreme Mechanics Letters},
% volume={19},
% pages={27--38},
% year={2018},
% publisher={Elsevier}
% }

% PliablePart is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License.
%
% PliablePart is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with RigidPart.
% If not, see <http://www.gnu.org/licenses/>.
function [sigmaint,dsigmaintdu]=cohesivelaw(u,sigmamax,phitot)
% Function computes the radial interfacial stress (sigmaint) 
% & its derivative w.r.t the radial interface displacement (dsigmaintdu) 
% for a given radial interface displacement u, and interface properties
% (sigmamax,phitot)
%
% Input
%   u           - interfacial debond
%   sigmamax    - maximum sigma
%   phitot      - total energy
% Output
%   sigmaint    - radial interfacial traction
%   dsigmaintdu - derivative of sigmaint wrt u


mu=phitot/(sigmamax*exp(1));
sigmaint=sigmamax*(u/mu).*exp(1-u/mu);
dsigmaintdu=(sigmamax/mu)*exp(1-u/mu).*(1.0-u/mu);