% Copyright 2017 Mechanics for Material Design Lab,
% Sibley School of Mechanical and Aerospace Engineering,
% Cornell, Ithaca
% Author: Meenakshi Sundaram
% Contact: mm2422 at cornell dot edu

% This file is part of RadialSymmetricPull package that solves the model in the paper
% "Theoretical framework and design of mechanochemically augmented polymer composites"
% by Meenakshi Sundaram Manivannan and Meredith N. Silberstein
% Cite the work using the following Bibtex entry
% @article{manivannan2018theoretical,
% title={Theoretical framework and design of mechanochemically augmented polymer composites},
% author={Manivannan, Meenakshi Sundaram and Silberstein, Meredith N},
% journal={Extreme Mechanics Letters},
% volume={19},
% pages={27--38},
% year={2018},
% publisher={Elsevier}
% }

% PliablePart is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License.
%
% PliablePart is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with RigidPart.
% If not, see <http://www.gnu.org/licenses/>.
function [rhs,drhsdu]=eqnfordebonddisp(u,matprop,geomprop,sigmas)
% function provides the right hand side of the equation that 
% finds the difference between the radial interface displacement "u" 
% to the interface displacement of the infinite matrix. This should be
% solved for "u" such that the right hand side is zero. The derivative of
% the right hand side with respect to "u" is also provided. 
%
% The system whose diplacements are solved for is an infinite matrix with a
% pliable circular particle subjective to a cohesive zone law. This system is 
% remotely loaded by a stress state. The interface has a varying Young's 
% modulus forming an interphase.
%
%Input
%   u           -   debonding displacement
%   matprop     -   material properties 
%               [1,2,3,4,5,6,7]
%               1. Ea      -   Youngs modulus of Interphase at ra
%               2. Ef      -   Youngs modulus of fiber
%               3. n       -   power law coefficient n
%               4. num     -   poisson's ratio of matrix   
%               5. nuf     -   poisson's ratio of fiber
%               6. sigmamax-   max stress of the cohesive law of the interface
%               7. phitot  -   energy of the cohesive law of the interface 
%   geomprop    -   geometric properties
%               [1,2]
%               ra      -   radius at the beginning of Interphase
%               rb      -   radius at the end of Interphase
%   sigmas      -   stresses involved
%               [1,2]
%               sigmaz  -   stress along z direction
%               sigmab  -   stress along the radial direction
%Output
%   rhs         -   output of the equation which we want to zero
%   drhs        -   derivative of the equation with respect u



Ea=matprop(1);Ef=matprop(2);n=matprop(3);num=matprop(4);nuf=matprop(5);sigmamax=matprop(6);phitot=matprop(7);
ra=geomprop(1);rb=geomprop(2);
sigmaz=sigmas(1);sigmab=sigmas(2);

% Obtain sigmaint and derivative
[sigmaint,dsigmaintdu]=cohesivelaw(u,sigmamax,phitot);

% Parametrize k
k=sqrt(n*n+4.0*(1-n*num));

% Formulation of the equation
A2=rb^((n+k)/2.0-1.0)*2.0*(sigmaint*(1.0-num*num)-sigmaz*(1.0+num)*num)*(k-n+2.0)...
    -ra^((n+k)/2.0-1.0)*4.0*(sigmab*(1.0-num)-sigmaz*num)*(k-n+2.0*num);
dA2du=rb^((n+k)/2.0-1.0)*2.0*(dsigmaintdu*(1.0-num*num))*(k-n+2);
A2=A2/Ea;
dA2du=dA2du/Ea;
A2=A2/(ra^(-(n+k)/2.0-1.0)*rb^((n+k)/2.0-1.0)*(-k-n+2.0*num)*(k-n+2.0)...
    -ra^((-n+k)/2.0-1.0)*rb^((n-k)/2.0-1.0)*(k-n+2.0*num)*(-k-n+2.0));
dA2du=dA2du/(ra^(-(n+k)/2.0-1.0)*rb^((n+k)/2.0-1.0)*(-k-n+2.0*num)*(k-n+2.0)...
    -ra^((-n+k)/2.0-1.0)*rb^((n-k)/2.0-1.0)*(k-n+2.0*num)*(-k-n+2.0));

A1=4.0*(ra^n)*(sigmab*(1.0-num)-sigmaz*num)-Ea*A2*rb^((n-k)/2.0-1.0)*(-k-n+2.0);
dA1du=-Ea*dA2du*rb^((n-k)/2.0-1.0)*(-k-n+2.0);
A1=A1/(Ea*rb^((n+k)/2.0-1.0)*(k-n+2.0));
dA1du=dA1du/(Ea*rb^((n+k)/2.0-1.0)*(k-n+2.0));

rhs=u-A1*ra^((k-n)/2.0)-A2*ra^(-(k+n)/2.0)+ra*((1.0-nuf)*sigmaint-nuf*sigmaz)/Ef;
drhsdu=1.0-dA1du*ra^((k-n)/2.0)-dA2du*ra^(-(k+n)/2.0)+ra*((1.0-nuf)*dsigmaintdu)/Ef;

