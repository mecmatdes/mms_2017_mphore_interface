% Copyright 2017 Mechanics for Material Design Lab,
% Sibley School of Mechanical and Aerospace Engineering,
% Cornell, Ithaca
% Author: Meenakshi Sundaram
% Contact: mm2422 at cornell dot edu

% This file is part of RadialSymmetricPull package that solves the model in the paper
% "Theoretical framework and design of mechanochemically augmented polymer composites"
% by Meenakshi Sundaram Manivannan and Meredith N. Silberstein
% Cite the work using the following Bibtex entry
% @article{manivannan2018theoretical,
% title={Theoretical framework and design of mechanochemically augmented polymer composites},
% author={Manivannan, Meenakshi Sundaram and Silberstein, Meredith N},
% journal={Extreme Mechanics Letters},
% volume={19},
% pages={27--38},
% year={2018},
% publisher={Elsevier}
% }

% PliablePart is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License.
%
% PliablePart is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with RigidPart.
% If not, see <http://www.gnu.org/licenses/>.
function [act,whichact,phisamples]=compuactiv(sigmas,matprop,geomprop,...
    mphoreprop,debond,coeffs,numsamples,phisamples)
% Function computes mechanophore activation at each location given the
% loading condition, material properties, the solved elasticity equation
% 
% The system whose diplacements are solved for is an infinite matrix with a
% pliable circular particle subjective to a cohesive zone law. This system is 
% remotely loaded by a stress state. The interface has a varying Young's 
% modulus forming an interphase.
%
%
%Input
%   sigmas      -   Loading
%               1.  sigma_z - z direction loading
%               2.  sigma   - hydrostatic loading
%   matprop     -   material properties 
%               [1,2,3,4,5,6,7
%               1. E(r_p^+) -   Youngs modulus of Interphase at ra
%               2. E_p      -   Youngs modulus of particle
%               3. n        -   power law coefficient n
%               4. nu_m     -   poisson's ratio of matrix   
%               5. nu_p     -   poisson's ratio of particle
%               6. sigma_max-   max stress of the cohesive law of the interface
%               7. phitot   -   energy of the cohesive law of the interface 
%   geomprop    -   geometric properties
%               1. r_p      -   radius at the beginning of Interphase
%               2. r_m      -   radius at the end of Interphase
%   mphoreprop  -   mechanophore properties 
%               1. l_na     -   non activated mechanophore length
%               2. l_a      -   activated mechanophore length
%               3. r_a      -   attachment radius of mechanophore < r_p
%   debond      -   radial debonding displacement
%   coeffs      -   Coefficients to reproduce displacements and stresses in
%                   a struct
%               1. As
%               2. Abars
%               3. Cs
%               4. Cbars
%Output
%   act         -  average number of mechanophores that activated
%   whichact    -  which all mechanophores activated
%   phisamples  -  Samples of angles between the two extreme +-phimax 

l_na=mphoreprop(1);l_a=mphoreprop(2);r_a=mphoreprop(3);
r_p=geomprop(1);

E_p=matprop(2);nu_p=matprop(5);sigma_max=matprop(6);phitot=matprop(7);

% If phisamples not given
if(nargin<8)
    phi_max=acos((r_p*r_p-l_na*l_na-r_a*r_a)/(2.0*r_a*l_na));
    phisamples=linspace(-phi_max,phi_max,numsamples);
end
thetasamples=atan(l_na*sin(phisamples)./(l_na*cos(phisamples)+r_a));
radiussamples=sqrt((l_na*sin(phisamples)).^2+(l_na*cos(phisamples)+r_a).^2);

% sigma interface
[sigma_int,~]=cohesivelaw(debond,sigma_max,phitot);

[disp,~]=dispstresseval(matprop,geomprop,sigmas,coeffs.As,coeffs.Abars,coeffs.Cs,coeffs.Cbars,radiussamples);
part_def=r_a*(1.0+(1.0-nu_p)*sigma_int/E_p);
whichact=(((radiussamples+disp).*(cos(thetasamples))-part_def).^2+((radiussamples+disp).*(sin(thetasamples))).^2)>(l_a*l_a);
act=sum(whichact)/numsamples;


