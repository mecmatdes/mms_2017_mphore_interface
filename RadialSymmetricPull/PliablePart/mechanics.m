% Copyright 2017 Mechanics for Material Design Lab,
% Sibley School of Mechanical and Aerospace Engineering,
% Cornell, Ithaca
% Author: Meenakshi Sundaram
% Contact: mm2422 at cornell dot edu

% This file is part of RadialSymmetricPull package that solves the model in the paper
% "Theoretical framework and design of mechanochemically augmented polymer composites"
% by Meenakshi Sundaram Manivannan and Meredith N. Silberstein
% Cite the work using the following Bibtex entry
% @article{manivannan2018theoretical,
% title={Theoretical framework and design of mechanochemically augmented polymer composites},
% author={Manivannan, Meenakshi Sundaram and Silberstein, Meredith N},
% journal={Extreme Mechanics Letters},
% volume={19},
% pages={27--38},
% year={2018},
% publisher={Elsevier}
% }

% PliablePart is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License.
%
% PliablePart is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with RigidPart.
% If not, see <http://www.gnu.org/licenses/>.
function [debond,coeffs]=mechanics(sigmas,matprop,geomprop,uguess)
% Function solves the mechanics problem for the system with a pliable circular particle 
% in an infinite matrix and where the interface is governed by a cohesive zone law. This system is 
% remotely loaded by a stress state. The interface has a varying Young's 
% modulus forming an interphase.
%
%Input
%   sigmas      -   Loading
%               1.  sigma_z - z direction loading
%               2.  sigma   - hydrostatic loading
%   matprop     -   material properties 
%               [1,2,3,4,5,6,7
%               1. Ea      -   Youngs modulus of Interphase at ra
%               2. Ef      -   Youngs modulus of fiber
%               3. n       -   power law coefficient n
%               4. num     -   poisson's ratio of matrix   
%               5. nuf     -   poisson's ratio of fiber
%               6. sigmamax-   max stress of the cohesive law of the interface
%               7. phitot  -   energy of the cohesive law of the interface 
%   geomprop    -   geometric properties
%               1. ra      -   radius at the beginning of Interphase
%               2. rb      -   radius at the end of Interphase
%   uguess      -   guess interfacial displacement
%Output
%   debond      -   Debonding displacement
%   coeffs      -   Coefficients to reproduce displacements and stresses in
%                   a struct
%               1. As
%               2. Abars
%               3. Cs
%               4. Cbars

% Setup function for solving for debond
func=@(u)eqnfordebonddisp(u,matprop,geomprop,sigmas);
options = optimoptions(@fsolve,'Display','off','Jacobian','on','TolX',1e-14,'TolFun',1e-14,'Algorithm','levenberg-marquardt');
count=-1;exitflag=-1;
if(nargin==4)
    [debond,~,exitflag,~] = fsolve(func,uguess,options);
end
while(exitflag<1)
    count=count+1;
    [debond,~,exitflag,~] = fsolve(func,count*rand,options);
end

% Coefficients for displacements
[As,Abars,Cs,Cbars]=conseval(debond,matprop,geomprop,sigmas);

%Coefficients to reproduce displacements,stresses
coeffs=struct('As',As,'Abars',Abars,'Cs',Cs,'Cbars',Cbars);