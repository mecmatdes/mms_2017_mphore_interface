% Copyright 2017 Mechanics for Material Design Lab,
% Sibley School of Mechanical and Aerospace Engineering,
% Cornell, Ithaca
% Author: Meenakshi Sundaram
% Contact: mm2422 at cornell dot edu

% This file is part of UniaxialPull package that solves the model in the paper
% "Theoretical framework and design of mechanochemically augmented polymer composites"
% by Meenakshi Sundaram Manivannan and Meredith N. Silberstein
% Cite the work using the following Bibtex entry
% @article{manivannan2018theoretical,
% title={Theoretical framework and design of mechanochemically augmented polymer composites},
% author={Manivannan, Meenakshi Sundaram and Silberstein, Meredith N},
% journal={Extreme Mechanics Letters},
% volume={19},
% pages={27--38},
% year={2018},
% publisher={Elsevier}
% }

% NoSlip is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License.
%
% NoSlip is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with UniaxialPull.
% If not, see <http://www.gnu.org/licenses/>.
function kernvalues=kernelvalues(A,B,C,nmodes)
%% Values of the kernel for nmodes
% Input
%   A, B, C     - Material parameters
%   nmodes      - Number of modes 
% Output
%   kernvalues  - Coeff values
%   kernvalues'=  [A   0]
%                 [.   .]
%                 [.   .]
%                 [.   .]
%                 [Bn Gn]
%                 [.   .]
%                 [.   .]
%                 [.   .]
%                           Where,
%                           Bn -- (B-modenum*C)/(modenum^2-1)
%                           Gn -- (C-modenum*B)/(modenum^2-1)
%%
kernvalues=zeros(2,nmodes);
kernvalues(1,1)=A;
den=(2:nmodes).*(2:nmodes)-1;
kernvalues(:,2:nmodes)=[B-(2:nmodes)*C;C-(2:nmodes)*B];
kernvalues(:,2:nmodes)=kernvalues(:,2:nmodes)./repmat(den,2,1);
