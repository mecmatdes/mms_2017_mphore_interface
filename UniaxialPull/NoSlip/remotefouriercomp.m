function [values]=remotefouriercomp(rstress,nmodes)
%% Integrations required for the remote load effect on the external kernel
% Input
%   rstress     -   remote stress (column 3x1)
%                       [sigma_xx;sigma_yy;sigma_xy]
%   nmodes      -   number of modes
%
% Output
%   values      -   values of the integrations of the tractions times the
%                   fourier components
%                   -[sigma_xx,sigma_xy;sigma_xy,sigma_yy]*[cos(theta);sin(theta)]*[1 cos(theta) sin(theta) cos(2 theta) sin(2 theta) ...]
%                   - only
%                   [sigma_xx*cos(theta)*cos(theta);sigma_xy*cos(theta)*cos(theta)],[sigma_xy*sin(theta)*sin(theta);sigma_yy*sin(theta)*sin(theta)]
%                   - will remain i.e., the second and third components
%%

values=integral(@intf,0,2*pi,'Reltol',1e-10,'AbsTol',1e-6,'ArrayValued',true);

    % Integration of tractions
    function valu=intf(theta)
        base=[cos(theta),sin(theta);...
            -sin(theta),cos(theta)]*[rstress(1),rstress(3);...
                                     rstress(3),rstress(2)]*[cos(theta);...
                                                             sin(theta)];
        valu=zeros(2,2*nmodes+1);
        valu(:,1)=base;
        valu(:,2:2:end)=base*cos((1:nmodes)*theta);
        valu(:,3:2:end)=base*sin((1:nmodes)*theta);
    end
  

end