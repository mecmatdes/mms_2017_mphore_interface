% Copyright 2017 Mechanics for Material Design Lab,
% Sibley School of Mechanical and Aerospace Engineering,
% Cornell, Ithaca
% Author: Meenakshi Sundaram
% Contact: mm2422 at cornell dot edu

% This file is part of UniaxialPull package that solves the model in the paper
% "Theoretical framework and design of mechanochemically augmented polymer composites"
% by Meenakshi Sundaram Manivannan and Meredith N. Silberstein
% Cite the work using the following Bibtex entry
% @article{manivannan2018theoretical,
% title={Theoretical framework and design of mechanochemically augmented polymer composites},
% author={Manivannan, Meenakshi Sundaram and Silberstein, Meredith N},
% journal={Extreme Mechanics Letters},
% volume={19},
% pages={27--38},
% year={2018},
% publisher={Elsevier}
% }

% NoSlip is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License.
%
% NoSlip is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with UniaxialPull.
% If not, see <http://www.gnu.org/licenses/>.
function [probact,pfact]=compuact(rp,mphoreprop,ucoeff,rstress,material,...
    trvalues,ttvalues,angularloc,numsamples)
%% Computes activation based on ucoeff, material properties, 
%% mphore properties, mphore end points, parameters
% Input
%   rp             - particle radius
%   mphoreprop     - mechanophore property
%       ra             - mechanophore mount point
%       lna            - nonactivated length
%       dell           - change in length required to activate
%   ucoeff         - radial coefficients dictating interfacial displacement
%   rstress        - remote stress applied [xx;yy;xy];
%   material
%                  - Lame parameters for matrix
%                        lam, mum 
%                  - Lame parameters for the particle
%                        lap, mup
%   trvalues       - interfacial traction along the radius
%   ttvalues       - interfacial traction along the tangent
%   angularloc     - angular location over which we need to measure
%   numsamples     - number of samples needed per theta location
% Output
%   probact        - The probability of activation
%   pfact          - The orientation factor for each angular position
%%

%% unpack
ra=mphoreprop(1);
lna=mphoreprop(2);
dell=mphoreprop(3);

lam=material(1);
mum=material(2);
lap=material(3);
mup=material(4);

%% Predeclare Fourier coefficients
ncoeff=length(ucoeff);
nmodes=(ncoeff-1.0)/2.0;

uoutcoeff=zeros(numsamples,ncoeff);
uincoeff=zeros(1,ncoeff);
voutcoeff=zeros(numsamples,ncoeff);
vincoeff=zeros(1,ncoeff);

%% Find samples of radii and angle stencil over which we need displacements
phi=acos((rp*rp-lna*lna-ra*ra)/(2.0*lna*ra));
phisamples=linspace(-phi+eps,phi-eps,numsamples);

radiussamples=sqrt((lna*sin(phisamples)).^2+(lna*cos(phisamples)+ra).^2);
thetasamples=atan(lna*sin(phisamples)./(lna*cos(phisamples)+ra));

%% Translate these to alphain and alphaout
alphain=ra/rp;
alphaout=radiussamples'/rp;

%% Integrals
[rvalues]=remotefouriercomp(rstress,nmodes);


%% remote strain
rstrain=(1.0/(4.0*mum*(lam+mum)))*[lam+2.0*mum,-lam,0.0;...
                                  -lam,lam+2.0*mum,0.0;...
                                  0.0,0.0,2.0*(lam+mum)]...
                                  *rstress;

%% Continuum displacemnt
consout=[(lam+mum*2.0)/(4.0*pi*mum*(lam+mum)),1.0/(4.0*pi*mum),1.0/(2.0*pi*(lam+mum))];
consin=[(lap+mup*2.0)/(4.0*pi*mup*(lap+mup)),1.0/(4.0*pi*mup),1.0/(2.0*pi*(lap+mup))];

%% Constant term
uoutcoeff(:,1)=(-trvalues(1)+rvalues(1,1))*(consout(1)./alphaout+consout(3)*(1.0./(2.0*alphaout)-1.0))...
    +alphaout*((rstrain(1)+rstrain(2))/2.0);
voutcoeff(:,1)=(rvalues(2,1)-ttvalues(1))*((consout(1)+consout(2))./alphaout+consout(3)*(1./(2.0*alphaout)-1.0));
uincoeff(1,1)=(consin(1)-consin(2))*alphain*trvalues(1)-consin(3)*(alphain*alphain/4.0)*ttvalues(1);
vincoeff(1,1)=consin(3)*alphain*alphain/4.0*trvalues(1)+consin(1)*(alphain*2.0)*ttvalues(1);

%% Cos(theta-theta') Multipliers
outcosmult_11=consout(1)*(1.0./(2.0*alphaout.^2)-2.0*log(alphaout))...
    -consout(2)*(1.0+1.0./(alphaout.^2))...
    -(consout(3)/2.0)*(1.0-1.0./(2.0*alphaout.^2));
outcosmult_12=pi*consout(3);
outcosmult_21=-pi*consout(3);
outcosmult_22=consout(1)*(1.0./(2.0*alphaout.^2)-2.0*log(alphaout))...
    -consout(2)*(1.0-(1.0./(alphaout.^2)-3.0+2.0*alphaout.^2)./(1.0-alphaout.^2))...
    -(consout(3)/2.0)*(1.0-1.0./(2.0*alphaout.^2));

incosmult_11=(consin(1)/2.0-consin(2)+consin(3)/4.0)*alphain.^2-consin(2);
incosmult_22=consin(1)*(alphain.^2/2.0)+consin(2)*(alphain.^2-3.0)+consin(3)*(alphain.^2/4.0);

%% Sin(theta-theta') Multipliers
outsinmult_11=-pi*consout(3);
outsinmult_12=-consout(1)*(1.0./(2.0*alphaout.^2)+2.0*log(alphaout))...
    -consout(2)*(1.0+(2.0*alphaout.^2+1.0./alphaout.^2-3.0)./(1-alphaout.^2))...
    -(consout(3)/4.0)*(2.0-1.0./(alphaout.^2));
outsinmult_21=consout(1)*(1.0./(2.0*alphaout.^2)+2.0*log(alphaout))...
    +consout(2)*(1.0-(2.0*alphaout.^2+1.0./(alphaout.^2)-3.0)./(1.0-alphaout.^2))...
    -(consout(3)/4.0)*(2.0-1.0./(alphaout.^2));
outsinmult_22=-pi*consout(3);
insinmult_21=consin(2)+(consin(1)/2.0+consin(2))*alphain.^2;
insinmult_12=consin(1)*(-alphain.^2)+consin(2)*(alphain.^2-3.0);

%% Cos(theta)terms
uoutcoeff(:,2)=outcosmult_11*(-trvalues(2)+rvalues(1,2))...
    -outsinmult_11*(-trvalues(3)+rvalues(1,3))...
    +outcosmult_12*(rvalues(2,2)-ttvalues(2))-outsinmult_12*(rvalues(2,3)-ttvalues(3));
uincoeff(1,2)=incosmult_11*(trvalues(2))-insinmult_12*(ttvalues(3));

voutcoeff(:,2)=outcosmult_21*(-trvalues(2)+rvalues(1,2))...
    -outsinmult_21*(-trvalues(3)+rvalues(1,3))...
    +outcosmult_22*(rvalues(2,2)-ttvalues(2))-outsinmult_22*(rvalues(2,3)-ttvalues(3));
vincoeff(1,2)=-insinmult_21*(trvalues(3))+incosmult_22*(ttvalues(2));

%% Sin(theta) terms
uoutcoeff(:,3)=outcosmult_11*(-trvalues(3)+rvalues(1,3))...
    +outsinmult_11*(-trvalues(2)+rvalues(1,2))...
    +outcosmult_12*(rvalues(2,3)-ttvalues(3))+outsinmult_12*(rvalues(2,2)-ttvalues(2));
uincoeff(1,3)=incosmult_11*(trvalues(3))+insinmult_12*(ttvalues(2));

voutcoeff(:,3)=outcosmult_21*(-trvalues(3)+rvalues(1,3))...
    +outsinmult_21*(-trvalues(2)+rvalues(1,2))...
    +outcosmult_22*(rvalues(2,3)-ttvalues(3))+outsinmult_22*(rvalues(2,2)-ttvalues(2));
vincoeff(1,3)=insinmult_21*(trvalues(2))+incosmult_22*(ttvalues(3));

for iind = 2:nmodes

    %   Cos(n(theta-theta')) Multipliers
    tmp1=1.0./((iind+1.0)*(alphaout.^(iind+1.0)))+1.0./((iind-1.0)*(alphaout.^(iind-1.0)));
    tmp2=(alphaout./(1.0-alphaout.^2)).*(2.0./(alphaout.^iind)-1.0./(alphaout.^(iind-2.0))-1.0./(alphaout.^(iind+2.0)));
    tmp3=(2.0-1.0./(alphaout.^(iind-1.0))).*(1.0./(2.0*(iind-1.0)))-(2.0-1.0./(alphaout.^(iind+1.0))).*(1.0./(2.0*(iind+1.0)));
    outcosmult_11=consout(1)*tmp1+consout(2)*tmp2+consout(3)*tmp3;
    outcosmult_22=consout(1)*tmp1-consout(2)*tmp2+consout(3)*tmp3;    

    tmp1=alphain.^(iind+1.0)/(iind+1.0)+alphain.^(iind-1.0)/(iind-1.0);
    tmp2=(alphain./(alphain.^2-1.0)).*(2.0*alphain.^(iind)-alphain.^(iind-2.0)-alphain.^(iind+2.0));
    tmp3=(alphain.^(iind+1.0)/(2.0*(iind+1.0))-alphain.^(iind-1.0)/(2.0*(iind-1.0)));
    incosmult_11=consin(1)*tmp1+consin(2)*tmp2+consin(3)*tmp3;
    incosmult_22=consin(1)*tmp1-consin(2)*tmp2+consin(3)*tmp3;
    
    %   Sin(n(theta-theta')) Multipliers
    tmp1=1.0./((iind-1.0)*(alphaout.^(iind-1.0)))-1.0./((iind+1.0)*(alphaout.^(iind+1.0)));
    tmp2=(alphaout./(1.0-alphaout.^2)).*(2.0./(alphaout.^iind)-1.0./(alphaout.^(iind-2.0))-1.0./(alphaout.^(iind+2.0)));
    tmp3=(2.0-1.0./(alphaout.^(iind-1.0))).*(1.0./(2.0*(iind-1.0)))+(2.0-1.0./(alphaout.^(iind+1.0))).*(1.0./(2.0*(iind+1.0)));
    outsinmult_12=consout(1)*tmp1+consout(2)*tmp2+consout(3)*tmp3;
    outsinmult_21=-consout(1)*tmp1+consout(2)*tmp2-consout(3)*tmp3;     
    
    tmp1=alphain.^(iind-1.0)/(iind-1.0)-alphain.^(iind+1.0)/(iind+1.0);
    tmp2=(alphain./(alphain.^2-1.0)).*(2.0*alphain.^(iind)-alphain.^(iind-2.0)-alphain.^(iind+2.0));
    tmp3=(alphain.^(iind-1.0)/(2.0*(iind-1.0))+alphain.^(iind+1.0)/(2.0*(iind+1.0)));
    insinmult_21=-consin(1)*tmp1-consin(2)*tmp2+consin(3)*tmp3;
    insinmult_12=consin(1)*tmp1-consin(2)*tmp2-consin(3)*tmp3;
    
    %  Cos Coefficients
    uoutcoeff(:,2*iind)=outcosmult_11*(-trvalues(2*iind)+rvalues(1,2*iind))...
        -outsinmult_12*(rvalues(2,2*iind+1)-ttvalues(2*iind+1));
    
    voutcoeff(:,2*iind)=-outsinmult_21*(-trvalues(2*iind+1)+rvalues(1,2*iind+1))...
        +outcosmult_22*(rvalues(2,2*iind)-ttvalues(2*iind));
    
    uincoeff(1,2*iind)=incosmult_11*(trvalues(2*iind))-insinmult_12*(ttvalues(2*iind+1));
    vincoeff(1,2*iind)=-insinmult_21*(trvalues(2*iind+1))+incosmult_22*(ttvalues(2*iind));
    
    %  Sin Coefficients
    uoutcoeff(:,2*iind+1)=outcosmult_11*(-trvalues(2*iind+1)+rvalues(1,2*iind+1))...
        +outsinmult_12*(rvalues(2,2*iind)-ttvalues(2*iind));
    
    voutcoeff(:,2*iind+1)=outsinmult_21*(-trvalues(2*iind)+rvalues(1,2*iind))...
        +outcosmult_22*(rvalues(2,2*iind+1)-ttvalues(2*iind+1));
    
    uincoeff(1,2*iind+1)=incosmult_11*(trvalues(2*iind+1))+insinmult_12*(ttvalues(2*iind));
    vincoeff(1,2*iind+1)=insinmult_21*(trvalues(2*iind))+incosmult_22*(ttvalues(2*iind+1));
    
end

uoutcoeff(:,4)=uoutcoeff(:,4)+alphaout*(rstrain(1)-rstrain(2))/2.0;
uoutcoeff(:,5)=uoutcoeff(:,5)+alphaout*rstrain(3);
voutcoeff(:,4)=voutcoeff(:,4)+alphaout*rstrain(3);
voutcoeff(:,5)=voutcoeff(:,5)+alphaout*(rstrain(2)-rstrain(1))/2.0;


% finding displacements of the particle at each angularloc
uindisp=rp*fouriereval(uincoeff,angularloc);
vindisp=rp*fouriereval(vincoeff,angularloc);

% new position inside
ix=(ra+uindisp).*cos(angularloc)-vindisp.*sin(angularloc);
iy=(ra+uindisp).*sin(angularloc)+vindisp.*cos(angularloc);

% Outside positions
activ=zeros(size(ix));  
pfact=zeros(size(ix));
count=zeros(size(ix));
for iind = 1:numsamples
        orientset=angularloc+thetasamples(iind);
        uoutdisp=rp*fouriereval(uoutcoeff(iind,:),orientset);   
        voutdisp=rp*fouriereval(voutcoeff(iind,:),orientset);        
        ox=(radiussamples(iind)+uoutdisp).*cos(orientset)-voutdisp.*sin(orientset);
        oy=(radiussamples(iind)+uoutdisp).*sin(orientset)+voutdisp.*cos(orientset);
        tmp=((sqrt((ox-ix).^2+(oy-iy).^2)-lna)>=dell);
        loc=(tmp==1);
        if(sum(loc)>0)
            pfact(loc)=pfact(loc)+cos(phisamples(iind)).^2;
            count(loc)=count(loc)+1;
        end
        activ=activ+tmp;
end
% 
probact=activ/(numsamples);
loc=pfact>0;
pfact(loc)=2*(pfact(loc)./count(loc))-1.0;