% Copyright 2017 Mechanics for Material Design Lab,
% Sibley School of Mechanical and Aerospace Engineering,
% Cornell, Ithaca
% Author: Meenakshi Sundaram
% Contact: mm2422 at cornell dot edu

% This file is part of UniaxialPull package that solves the model in the paper
% "Theoretical framework and design of mechanochemically augmented polymer composites"
% by Meenakshi Sundaram Manivannan and Meredith N. Silberstein
% Cite the work using the following Bibtex entry
% @article{manivannan2018theoretical,
% title={Theoretical framework and design of mechanochemically augmented polymer composites},
% author={Manivannan, Meenakshi Sundaram and Silberstein, Meredith N},
% journal={Extreme Mechanics Letters},
% volume={19},
% pages={27--38},
% year={2018},
% publisher={Elsevier}
% }

% NoSlip is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License.
%
% NoSlip is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with UniaxialPull.
% If not, see <http://www.gnu.org/licenses/>.
%%
% This file generates the figures 5.a and 5.b in the paper. 

%% Clear and Close
clc;clear;close all;

%% Material Properties
% Interface Param
SigmaMax=0.15; % Non dimensionalized by Em
deltan=0.75;%nm

% Particle Youngs Modulus/Poissons ratio
Ep=500e3; %MPa
nup=0.3; 
Rp=13+1/3; % Non dimensionalized by deltan

% Continuum
Em=800;%MPa,
num=0.3;

% Dimensionalize
rp=Rp*deltan; %nm
sigmamax=SigmaMax*Em; %MPa

% Converting to lame properties
[lam,mum]=Enu2lame(Em,num);
[lan,mun]=Enu2lame(Ep,nup);

% material properties assembled 
materialprop=[lam,mum,lan,mun];

% Constants inferred from material properties
[A,B,C]=constalgebr(materialprop);

% interface parameters assembled
intparam=[sigmamax,deltan/rp,25];

% Mechanophore properties
ra=rp-0.1;%nm
Lna=4.0/3.0;
DeltaL=0.2;
lna=Lna*deltan;
dell=DeltaL*deltan;
mphoreprop=[ra,lna,dell];

%% Number of modes
nmodes=35;

%% Kernel Eigenvalues
kernvalues=kernelvalues(A,B,C,nmodes);

%% Loading cycle - remote stress details
nsim=5;
nsamples=256;
theta=linspace(-pi/2,pi/2,nsamples);

loadst=[0.02,0.05,0.08,0.11,0.13];
ucoeff=zeros(nsim,2*nmodes+1);
vcoeff=zeros(nsim,2*nmodes+1);

uval=zeros(1,nsim);
maxind=nsim;

probact=zeros(maxind,nsamples);
pfact=zeros(maxind,nsamples);

ninsamp=10;
noutsamp=10;

uindisp=zeros(ninsamp,nsamples,nsim);
vindisp=zeros(ninsamp,nsamples,nsim);

uoutdisp=zeros(noutsamp,nsamples,nsim);
voutdisp=zeros(noutsamp,nsamples,nsim);

radiusinloc=linspace(0.1,rp,ninsamp);
radiusoutloc=linspace(rp,5.0*rp,noutsamp);
angularloc=linspace(0,2*pi,nsamples);
%%
for iind = 1:maxind

    display([iind])
    rstress=[loadst(iind);0;0]*Em;%MPa
    
    [hr,ht]=remoteload([lam,mum],rstress);
    func=@(coeffi)algebraiceqn(coeffi,kernvalues,intparam,hr,ht);
    options = optimoptions('fsolve','Algorithm','levenberg-marquardt','InitDamping',0.1,...
        'Jacobian','on','display','off','MaxIter',80);%'DerivativeCheck','off','FinDiffType','central');
    
    if(iind==1)
        ucoeff(iind,1:5)=hr*1e-3;
        [soln,fval,exitflag,output]=fsolve(func,ucoeff(iind,:),options);
    else
        [soln,fval,exitflag,output]=fsolve(func,ucoeff(iind-1,:),options);
    end
    
    count=1;
    while(exitflag<1)
        count=count+1;
        options = optimoptions('fsolve','Algorithm','levenberg-marquardt','InitDamping',0.01,...
        'Jacobian','on','display','off','MaxIter',60);%'DerivativeCheck','off','FinDiffType','central');
        [soln,fval,exitflag,output]=fsolve(func,soln+count*1e-2*rand(1,2*nmodes+1),options);
        
    end

    ucoeff(iind,:)=soln;
    ucoeff(iind,2:3)=0;
    vcoeff(iind,:)=0;
    uval(iind)=rp*fouriereval(ucoeff(iind,:),0);
    
    [trvalues,~]=intealgebr(ucoeff(iind,:),intparam);
    ttvalues=tangintealgebr(trvalues,kernvalues,ht);
    
    [probact(iind,:),pfact(iind,:)]=compuact(rp,mphoreprop,ucoeff(iind,:),rstress,materialprop,trvalues,ttvalues,theta,nsamples);
    [uindisp(:,:,iind),vindisp(:,:,iind),uoutdisp(:,:,iind),voutdisp(:,:,iind)]=...
        compudisp(rp,ucoeff(iind,:),rstress,materialprop,trvalues,ttvalues,angularloc,radiusinloc,radiusoutloc);
end
%%
fsize=32;lsize=4;
h=figure(1);clf;
loadcas=1:nsim;
lin1=plot(theta*180/pi,probact(loadcas,:)','linewidth',lsize);
hold on;
lin2=plot(theta*180/pi,probact(loadcas,:)','linewidth',lsize);
for iind = 1:5
    lin2(iind).Color=lin1(iind).Color;
end

set(gca,'fontsize',fsize);
xlabel('$\theta$','fontsize',fsize,'interpreter','LaTeX');
ylabel('$\mathcal{E}(\theta)$','fontsize',fsize,'interpreter','LaTeX','Rotation',0);
axis([-90,90,0,1])
axis square;
hl=legend(num2str(round(loadst(loadcas(1)),2)),num2str(round(loadst(loadcas(2)),2)),num2str(round(loadst(loadcas(3)),2)),...
    num2str(round(loadst(loadcas(4)),2)),num2str(round(loadst(loadcas(5)),2)));
set(hl,'location','northeast');
set(gca,'XTick',[-90,-45,0,45,90])
print(h,'figures/noslipact.svg','-dsvg')
%%
udisp1=zeros(nsim,nsamples);
vdisp1=zeros(nsim,nsamples);
for iind = 1:nsim
    udisp1(iind,:)=rp*fouriereval(ucoeff(iind,:),theta);
    vdisp1(iind,:)=rp*fouriereval(vcoeff(iind,:),theta);
end
rdisp1=sqrt(udisp1.^2+vdisp1.^2);
%%
fsize=32;lsize=4;asize=2;
h=figure(2);clf;
plot(theta*180/pi,rdisp1(loadcas,:)'/deltan,'linewidth',lsize)
set(gca,'fontsize',fsize);
xlabel('$\theta$','fontsize',fsize,'interpreter','LaTeX');
ylabel('$\left[D\right]$','fontsize',fsize,'interpreter','LaTeX','Rotation',0);
axis tight square;
hl=legend(num2str(round(loadst(loadcas(1)),2)),num2str(round(loadst(loadcas(2)),2)),num2str(round(loadst(loadcas(3)),2)),...
    num2str(round(loadst(loadcas(4)),2)),num2str(round(loadst(loadcas(5)),2)));
set(hl,'location','northeast');
set(gca,'XTick',[-90,-45,0,45,90])
print(h,'figures/noslipdisp.svg','-dsvg')
