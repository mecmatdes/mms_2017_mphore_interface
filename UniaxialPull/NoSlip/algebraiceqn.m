% Copyright 2017 Mechanics for Material Design Lab,
% Sibley School of Mechanical and Aerospace Engineering,
% Cornell, Ithaca
% Author: Meenakshi Sundaram
% Contact: mm2422 at cornell dot edu

% This file is part of UniaxialPull package that solves the model in the paper
% "Theoretical framework and design of mechanochemically augmented polymer composites"
% by Meenakshi Sundaram Manivannan and Meredith N. Silberstein
% Cite the work using the following Bibtex entry
% @article{manivannan2018theoretical,
% title={Theoretical framework and design of mechanochemically augmented polymer composites},
% author={Manivannan, Meenakshi Sundaram and Silberstein, Meredith N},
% journal={Extreme Mechanics Letters},
% volume={19},
% pages={27--38},
% year={2018},
% publisher={Elsevier}
% }

% NoSlip is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License.
%
% NoSlip is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with UniaxialPull.
% If not, see <http://www.gnu.org/licenses/>.
function [rhs,jacob]=algebraiceqn(ucoeff,kernvalues,param,hr,ht)
%% Function returns rhs of the algebraic equation
%   Input:
%       ucoeff      - Fourier Coefficients for radial direction
%                       array [1 x (2*nmodes+1)]
%       kernvalues  - Function "kernelvalues" gives these constants
%       param       - Parameters for the law
%                       sigma_max   - max stress radially
%                       delta_r     - radial critical length
%                       beta        - negative stiffening param
%       hr, ht      - Remote stress on a continuum with a void gives rise
%                       to these displacements prescribed by these fourier
%                       coefficients
%   Output:
%       rhs         - Right hand side equation
%       jacob       - Jacobian of the right hand side of the equation
%% 
ncoeffs=length(ucoeff);
nmodes=(ncoeffs-1.0)/2.0;
% 
rhs=zeros(ncoeffs,1);
%columns of jacob denote
%u_const u_cos u_sin u_cos_2 u_sin_2
jacob=zeros(ncoeffs,ncoeffs);
% Precoeff
pi2=2.0*pi;
% Integral - precalculate
[values,dvalues]=intealgebr(ucoeff,param);

%%
rhs(1)=pi2*(ucoeff(1)-hr(1)) - kernvalues(1,1)*values(1);
% Derivative wrt u_const u_cos u_sin u_cos_2 u_sin_2
jacob(1,:)=-kernvalues(1,1)*dvalues(1,:);
% Derivative wrt u_const
jacob(1,1)=jacob(1,1)+pi2;

%%
rhs(4)=(ucoeff(4)-hr(4)) + kernvalues(2,2)*ht(5)/kernvalues(1,2)-(kernvalues(1,2)-kernvalues(2,2)^2/kernvalues(1,2))*values(4)/(pi);
% Derivative wrt u_const u_cos u_sin u_cos_2 u_sin_2
jacob(4,:)=-(kernvalues(1,2)-kernvalues(2,2)^2/kernvalues(1,2))*dvalues(4,:)/(pi);
% Derivative wrt u_cos_2
jacob(4,4)=jacob(4,4)+1.0;

%%
rhs(5)=(ucoeff(5)-hr(5)) - kernvalues(2,2)*ht(4)/kernvalues(1,2)-(kernvalues(1,2)-kernvalues(2,2)^2/kernvalues(1,2))*values(5)/(pi);
% Derivative wrt u_const u_cos u_sin u_cos_2 u_sin_2
jacob(5,:)=-(kernvalues(1,2)-kernvalues(2,2)^2/kernvalues(1,2))*dvalues(5,:)/(pi);
% Derivative wrt u_cos_2
jacob(5,5)=jacob(5,5)+1.0;

%% Rest of the cases
coeffs=6:2:ncoeffs;
tmpvalues=kernvalues(1,3:nmodes)-kernvalues(2,3:nmodes).*kernvalues(2,3:nmodes)./kernvalues(1,3:nmodes);
rhs(coeffs)=ucoeff(coeffs) - (tmpvalues).*values(coeffs)/(pi);
% Derivative wrt u_const u_cos u_sin u_cos_2 u_sin_2
jacob(coeffs,:)=-spdiags(tmpvalues',0,nmodes-2,nmodes-2)*dvalues(coeffs,:)/(pi); 
% Derivative wrt v_sin_2
jacob(coeffs,coeffs)=jacob(coeffs,coeffs)+1.0*eye(nmodes-2);

%%
coeffs=7:2:ncoeffs;
tmpvalues=kernvalues(1,3:nmodes)-kernvalues(2,3:nmodes).*kernvalues(2,3:nmodes)./kernvalues(1,3:nmodes);
rhs(coeffs)=ucoeff(coeffs) - (tmpvalues).*values(coeffs)/(pi);
% Derivative wrt u_const u_cos u_sin u_cos_2 u_sin_2
jacob(coeffs,:)=-spdiags(tmpvalues',0,nmodes-2,nmodes-2)*dvalues(coeffs,:)/(pi); 
% Derivative wrt v_sin_2
jacob(coeffs,coeffs)=jacob(coeffs,coeffs)+1.0*eye(nmodes-2);
