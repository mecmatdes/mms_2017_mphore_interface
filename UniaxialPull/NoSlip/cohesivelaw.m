% Copyright 2017 Mechanics for Material Design Lab,
% Sibley School of Mechanical and Aerospace Engineering,
% Cornell, Ithaca
% Author: Meenakshi Sundaram
% Contact: mm2422 at cornell dot edu

% This file is part of UniaxialPull package that solves the model in the paper
% "Theoretical framework and design of mechanochemically augmented polymer composites"
% by Meenakshi Sundaram Manivannan and Meredith N. Silberstein
% Cite the work using the following Bibtex entry
% @article{manivannan2018theoretical,
% title={Theoretical framework and design of mechanochemically augmented polymer composites},
% author={Manivannan, Meenakshi Sundaram and Silberstein, Meredith N},
% journal={Extreme Mechanics Letters},
% volume={19},
% pages={27--38},
% year={2018},
% publisher={Elsevier}
% }

% NoSlip is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License.
%
% NoSlip is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with UniaxialPull.
% If not, see <http://www.gnu.org/licenses/>.
function [traction]=cohesivelaw(theta,ucoeff,param)
%% Function finds the radial traction due to interfacial displacement specified by
% t_r=(phi_r/delta_r)*(u/delta_r)*exp(-u/delta_r)
%
% Input:
%   theta       - angular directions over which tractions are evaluated
%                   array [1 x nsamples]
%   ucoeff      - Fourier Coefficients for radial and tangential direction
%                   array [1 x (2*numdeg+1)]
%   param       - Parameters for the law
%                   sigma_max   - max stress radially
%                   delta_r     - radial critical length
%                   beta        - negative stiffening param
% Output:
%   traction    - radial traction
%                 array [1 x (2*numdeg+1)]
%%

% Unpack parameters
sigma_max=param(1);
delta_r=param(2);
beta=param(3);

% Number of samples of theta over which tractions need to be evaluated
nsamples=length(theta);

% Radial Interfacial Displacements
rdisp=fouriereval(ucoeff,theta);

% traction laws
traction=zeros(1,nsamples);
loc=(rdisp>=0);
traction(loc)=sigma_max*(rdisp(loc)/delta_r).*exp(1.0-rdisp(loc)/delta_r);
traction(~loc)=-sigma_max/beta*exp(1.0-rdisp(~loc)*beta/delta_r)+sigma_max*exp(1.0)/beta;