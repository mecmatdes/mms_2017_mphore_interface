% Copyright 2017 Mechanics for Material Design Lab,
% Sibley School of Mechanical and Aerospace Engineering,
% Cornell, Ithaca
% Author: Meenakshi Sundaram
% Contact: mm2422 at cornell dot edu

% This file is part of UniaxialPull package that solves the model in the paper
% "Theoretical framework and design of mechanochemically augmented polymer composites"
% by Meenakshi Sundaram Manivannan and Meredith N. Silberstein
% Cite the work using the following Bibtex entry
% @article{manivannan2018theoretical,
% title={Theoretical framework and design of mechanochemically augmented polymer composites},
% author={Manivannan, Meenakshi Sundaram and Silberstein, Meredith N},
% journal={Extreme Mechanics Letters},
% volume={19},
% pages={27--38},
% year={2018},
% publisher={Elsevier}
% }

% NoSlip is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License.
%
% NoSlip is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with UniaxialPull.
% If not, see <http://www.gnu.org/licenses/>.
function [values,dvalues]=intealgebr(ucoeff,param)
%% Integrations required for the algebraic equations
% Input
%   ucoeff      -   Fourier Coefficients for the radial direction
%                   array [1 x (2*numdeg+1)]
%   param       -   Parameters for the law
%                       sigma_max   - max stress radially
%                       delta_r     - radial critical length
%                       beta        - negative stiffening param
%
% Output
%   values      -   values of the integrations of the tractions times the
%                   fourier components
%                   t_r[1 cos(theta) sin(theta) cos(2 theta) sin(2 theta) ...]
%                   
%   dvalues     -   values of the integrations of the differentiation of
%                   the tractions with the fourier components
%                   dt_r/du [1          cos(theta)              sin(theta) 
%                   dt_r/du [cos(theta) cos(theta)cos(theta)    cos(theta)sin(theta)... 
%                   dt_r/du [sin(theta) sin(theta)cos(theta)    sin(theta)sin(theta)... 
%                    . . .
%%

nmodes=(length(ucoeff)-1)/2.0;

values=integral(@intf,0,2*pi,'Reltol',1e-8,'AbsTol',1e-6,'ArrayValued',true);
dvalues=integral(@dintf,0,2*pi,'Reltol',1e-8,'AbsTol',1e-6,'ArrayValued',true);

    % Integration of tractions
    function valu=intf(theta)
        base=cohesivelaw(theta,ucoeff,param);
        valu=zeros(1,2*nmodes+1);
        valu(1,1)=base;
        valu(1,2:2:end)=base*cos((1:nmodes)*theta);
        valu(1,3:2:end)=base*sin((1:nmodes)*theta);
    end
  
    % Integration of differentiation of tractions
    function dvalu=dintf(theta)
        base=dcohesivelaw(theta,ucoeff,param);
        dvalu=zeros(2*nmodes+1,2*nmodes+1);
        
        dvalu(1,1)=1;
        dvalu(1,2:2:end)=cos((1:nmodes)*theta);
        dvalu(1,3:2:end)=sin((1:nmodes)*theta);
        dvalu(2:2:end,1)=cos((1:nmodes)*theta)';
        dvalu(3:2:end,1)=sin((1:nmodes)*theta)';
        
        dvalu(2:2:end,2:2:end)=cos((1:nmodes)*theta)'*cos((1:nmodes)*theta);
        dvalu(2:2:end,3:2:end)=cos((1:nmodes)*theta)'*sin((1:nmodes)*theta);
        dvalu(3:2:end,2:2:end)=sin((1:nmodes)*theta)'*cos((1:nmodes)*theta);
        dvalu(3:2:end,3:2:end)=sin((1:nmodes)*theta)'*sin((1:nmodes)*theta);
        
        dvalu(:,:)=base*dvalu(:,:);%d tract_radial/d u 
    end

end