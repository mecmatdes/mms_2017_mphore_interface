function [value]=fouriereval(coeff,theta)
%% Function evaluates fourier coefficients on the sample points given by
% theta
% Input:
%   coeff - Fourier Coefficient, 2*n+1 number, odd set of coefficients
%       Order
%           [cons, cos(1 theta), sin(1 theta), cos(2 theta), sin(2 theta)
%                  cos(3 theta), sin(3 theta), cos(4 theta), sin(4 theta)
%                  cos(5 theta), sin(5 theta), cos(6 theta), sin(6 theta)
%                  ..., ..., ..., ...]
%   theta - Samples over which theta is evaluated
%  
% Output:
%   value - Evaluation of the fourier series on each of the thetas
%%


numcoeff=length(coeff);
numseries=(numcoeff-1)/2;

value=coeff(1)*ones(size(theta));

for iind = 1:numseries
    % cos component
    value=value+coeff(2*iind)*cos(iind*theta);
    % sin component
    value=value+coeff(2*iind+1)*sin(iind*theta);
end

