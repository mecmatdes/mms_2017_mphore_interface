% Copyright 2017 Mechanics for Material Design Lab,
% Sibley School of Mechanical and Aerospace Engineering,
% Cornell, Ithaca
% Author: Meenakshi Sundaram
% Contact: mm2422 at cornell dot edu

% This file is part of UniaxialPull package that solves the model in the paper
% "Theoretical framework and design of mechanochemically augmented polymer composites"
% by Meenakshi Sundaram Manivannan and Meredith N. Silberstein
% Cite the work using the following Bibtex entry
% @article{manivannan2018theoretical,
% title={Theoretical framework and design of mechanochemically augmented polymer composites},
% author={Manivannan, Meenakshi Sundaram and Silberstein, Meredith N},
% journal={Extreme Mechanics Letters},
% volume={19},
% pages={27--38},
% year={2018},
% publisher={Elsevier}
% }

% NoSlip is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License.
%
% NoSlip is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with UniaxialPull.
% If not, see <http://www.gnu.org/licenses/>.
function [A,B,C]=constalgebr(material)
%% Function to evaluate the constants associated with the algebraic equation
% These help write the eigenvalues quite easily
%
% Input
%   material
%       Lame parameters for matrix
%       lam
%       mum 
%       Lame parameters for the particle
%       lap
%       mup
% Output
%       A=-0.5*(1.0/mum + 1.0/(mup+lam_p))
%       B=0.5*(1.0/(lap+mup)-1.0/(lam+mum))
%       C=0.5*((lam+2.0*mum)/(mum*(lam+mum))+(lap+2.0*mup)/(mup*(lap+mup)))
%%

% Unpacking the material
lam=material(1);
mum=material(2);
lap=material(3);
mup=material(4);

A=-0.5*(1.0/mum + 1.0/(mup+lap));
B=0.5*(1.0/(lap+mup)-1.0/(lam+mum));
C=0.5*((lam+2.0*mum)/(mum*(lam+mum))+(lap+2.0*mup)/(mup*(lap+mup)));
