function [E,nu]=Lame2Enu(lam,mu)
%% Convert material properties Young's modulus, Poisson's ratio to Lame parameters
%   Input:
%      Lame parameters - lam, mu
%   Output:
%       E  - Youngs modulus
%       nu - Poisson's ratio
%%
E=(3.0*lam+2.0*mu)*mu/(lam+mu);
nu=lam/2.0/(lam+mu);
    
