% Copyright 2017 Mechanics for Material Design Lab,
% Sibley School of Mechanical and Aerospace Engineering,
% Cornell, Ithaca
% Author: Meenakshi Sundaram
% Contact: mm2422 at cornell dot edu

% This file is part of UniaxialPull package that solves the model in the paper
% "Theoretical framework and design of mechanochemically augmented polymer composites"
% by Meenakshi Sundaram Manivannan and Meredith N. Silberstein
% Cite the work using the following Bibtex entry
% @article{manivannan2018theoretical,
% title={Theoretical framework and design of mechanochemically augmented polymer composites},
% author={Manivannan, Meenakshi Sundaram and Silberstein, Meredith N},
% journal={Extreme Mechanics Letters},
% volume={19},
% pages={27--38},
% year={2018},
% publisher={Elsevier}
% }

% NoSlip is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License.
%
% NoSlip is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with UniaxialPull.
% If not, see <http://www.gnu.org/licenses/>.
function [ttvalues]=tangintealgebr(trvalues,kernvalues,ht)
%% Integrations required for the algebraic equations
% Input
%   trvalues    -   
%   kernvalues  -   
%   ht          -
%
% Output
%   ttvalues    -   values of the integrations of the tangential tractions
%                   times trig components
%%

ncoeffs=length(trvalues);
nmodes=(ncoeffs-1)/2.0;
ttvalues=zeros(1,ncoeffs);

%%
ttvalues(2)=-trvalues(3);
ttvalues(3)=trvalues(2);

ttvalues(4)=(-pi*ht(4)+kernvalues(2,2)*trvalues(5))/kernvalues(1,2);
ttvalues(5)=(-pi*ht(5)-kernvalues(2,2)*trvalues(4))/kernvalues(1,2);

coeffs=6:2:ncoeffs;
ttvalues(coeffs)=kernvalues(2,3:nmodes).*trvalues(coeffs+1)./kernvalues(1,3:nmodes);
ttvalues(coeffs+1)=-kernvalues(2,3:nmodes).*trvalues(coeffs)./kernvalues(1,3:nmodes);