% Copyright 2017 Mechanics for Material Design Lab,
% Sibley School of Mechanical and Aerospace Engineering,
% Cornell, Ithaca
% Author: Meenakshi Sundaram
% Contact: mm2422 at cornell dot edu

% This file is part of UniaxialPull package that solves the model in the paper
% "Theoretical framework and design of mechanochemically augmented polymer composites"
% by Meenakshi Sundaram Manivannan and Meredith N. Silberstein
% Cite the work using the following Bibtex entry
% @article{manivannan2018theoretical,
% title={Theoretical framework and design of mechanochemically augmented polymer composites},
% author={Manivannan, Meenakshi Sundaram and Silberstein, Meredith N},
% journal={Extreme Mechanics Letters},
% volume={19},
% pages={27--38},
% year={2018},
% publisher={Elsevier}
% }

% NoSlip is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License.
%
% NoSlip is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with UniaxialPull.
% If not, see <http://www.gnu.org/licenses/>.
function [hr,ht]=remoteload(lame,rstress)
%%  Function to determine constants defining
%   fourier coefficients defining deformation of a
%   matrix with a unit circular void 
%   Input
%       lame        -   lame parameters for the matrix
%                       [lambda,mu]
%       rstress     -   remote stress (column 3x1)
%                       [sigma_xx;sigma_yy;sigma_xy]
%
%   Output
%       
%       hr          -   radial
%       ht          -   tangential
%       Order of coefficients
%       [1, cost(theta), sin(theta), cos(2 theta), sin(2 theta)]
%%

% Constant 1
cons1=[lame(1)+lame(2),lame(1)+2*lame(2),...
        lame(1)+3*lame(2),lame(1)-lame(2),...
            lame(2)*(lame(1)+lame(2))];

% lamda+mu,lamda+2*mu,lamda+3*mu,lamda-mu,mu*(lamda+mu)

% Epsilon
repsi=([cons1(2),-lame(1),0.0;...
     -lame(1), cons1(2),0.0;...
      0.0, 0.0, 2.0*cons1(1)]/(4.0*cons1(5)))*rstress;

% Constant 2
cons2=[cons1(2)*cons1(3)/(2.0*cons1(5)),cons1(2)*cons1(4)/(2.0*cons1(5)),2.0*cons1(2)/cons1(1)];

% 
eV= [cons2(1),cons2(2),0.0;...
     cons2(2),cons2(1),0.0;...
     0.0,0.0,cons2(3)]*repsi;

hr=[(eV(1)+eV(2))/2.0,0.0,0.0,(eV(1)-eV(2))/2.0,eV(3)];
ht=[0.0,0.0,0.0,eV(3),(eV(2)-eV(1))/2.0];

